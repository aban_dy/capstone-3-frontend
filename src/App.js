import React from 'react';
import './App.css';
import styled from 'styled-components';
import {Route , Switch, useLocation, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

/*
--import functions where it contains all the functions of this app
--import states where it contains all the states for this app
*/
import Loader from './components/Loader';
import { AnimatePresence } from 'framer-motion';


/*
  TODOS:
  --Create global sidenav
  --Create Renting page where girls are displayed
  --Code splitting is a must
  --Login/Register page
  --Payment Integration(Stripe)
  --Print PDF for admin reports and user receipt
  --Dashboard should be broken grid
*/

const Landing = React.lazy(()=> import('./views/home/Landing'));
const Home = React.lazy(()=> import('./views/home/Home'));
const Login = React.lazy(()=> import('./views/login/Login'));
const Register = React.lazy(()=> import('./views/register/Register'));
const Rent = React.lazy(()=> import('./views/rent/Rent'));
const Dashboard = React.lazy(()=> import('./views/dashboard/Dashboard'));

// import Landing from './views/home/Landing';
// import Home from './views/home/Home';
// import Login from './views/login/Login';
// import Register from './views/register/Register';

const Container = styled.div`
  width : 100%;
  height : auto;
`;

toast.configure({
  autoClose: 3000,
  draggable: false,
  hideProgressBar: true,
  newestOnTop: true,
  closeOnClick: true
});
const App = () => {
  const location = useLocation();
  const history = useHistory();
  return (
  <Container>
    <React.Suspense fallback={ <Loader /> }>
    <AnimatePresence exitBeforeEnter >
        <Switch location={ location } key={ location.pathname } history={ history }>
            <Route exact path="/" render={ props => <Landing { ...props } /> }/>
            <Route path="/home" render={ props => <Home { ...props } /> }/>
            <Route path="/login" render={ props => <Login { ...props } /> }/>
            <Route path="/register" render={ props => <Register { ...props } /> }/>
            <Route exact path="/rent/:id" render={ props => <Rent { ...props } /> }/>
            <Route path="/dashboard" render={ props => <Dashboard { ...props } /> }/>
        </Switch>
      </AnimatePresence>
    </React.Suspense>
  </Container>
  );
}

export default App;
