import React from 'react';
import { motion } from 'framer-motion';
import styled from 'styled-components';
import { NavLink,useHistory } from 'react-router-dom';
import home from '../assets/icons/home.png';
import dashboard from '../assets/icons/dashboard.png';
import logout from '../assets/icons/logout.png';
import ReactTooltip from "react-tooltip";

/*
	TODOS:
	--tooltip
*/

const links = [
	{
		name : "Home",
		path : "/home",
		icon : `${ home }`
	},
	{
		name : "Dashboard",
		path : "/dashboard",
		icon : `${ dashboard }`
	}
]
//Styled components
const Nav = styled(motion.div)`
	height: 100vh;
	width: 150px;
	position: fixed;
	left: 0;
	bottom: 0;
	right: 0;
	top: 0;
	display : flex;
	flex-direction: column;
	align-items: center;
	background: rgba(255,255,255,.25);
	backdrop-filter : blur(1rem);
	overflow: hidden;
	z-index : 1000;
`;

const Icon = styled(motion.div)`
	display : flex;
	width : 100%;
	padding : 1rem;
	flex-direction : column;
	justify-content : center;
	align-items : center;
	padding : .5rem;
	text-align : center;

	& .link {
		width : 100%;
		text-decoration : none;
		display: flex;
		flex-direction : column;
		justify-content : center;
		align-items: center;
		padding : 1rem 0;
	}

	& .link.active {
		border-right : 1px solid var(--danger);
		width : 90%;
	}

	& img {
		width : var(--sm);
	}

	& strong {
		padding : .5rem;
		font-weight : 200;
		text-transform : uppercase;
		color : var(--primary);
	}
`;

const LogoutIcon = styled(motion.div)`
	margin-top : auto;
	margin-bottom: 1rem;
	display : flex;
	width : 100%;
	padding : 1rem;
	flex-direction : column;
	justify-content : center;
	align-items : center;
	padding : .5rem;
	text-align : center;

	& img {
		width : var(--sm);
	}

	& strong {
		padding : .5rem;
		font-weight : 200;
		text-transform : uppercase;
		color : var(--primary);
	}
`;


const SideNav = props => {
	let history = useHistory();
	return(
		<Nav
			whileHover={{
				x : 0,
				transition : {
					duration : .3,
				}
			}}
			initial={{x:-140}}
		>
			{links.map(link=>(
			<Icon key={ link.name } data-tip={ link.name }>
				<NavLink exact to={ link.path } className="link">
					<motion.img 
					src={link.icon} 
					alt={link.name}
					whileHover={{
						scale : 1.2,
						transition : { duration : .2 }
					}}
					/>
				</NavLink>
			</Icon>
			))}
			{ sessionStorage.token ? (
				<LogoutIcon
				onClick={()=> {
					sessionStorage.clear();
					history.push('/login');
				}}
				data-tip="Logout"
			>
				<motion.img 
					src={ logout }
					alt="logout"
					whileHover={{
						scale : 1.2,
						transition : { duration : .2 }
					}}
				/>
				<ReactTooltip effect="solid"/>
			</LogoutIcon>
				) : "" }
		</Nav>
	)
}

export default SideNav;