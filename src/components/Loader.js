import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

const LoadingContainer = styled.div`
	height : 100vh;
	width : 100vw;
	display : flex;
	background : var(--dark);
	justify-content : center;
	align-items : center;

`

const LoadingCircleContainer = styled(motion.div)`
		width : 10rem;
		height : 10rem;
		display : flex;
		align-items : center;
		justify-content : space-around;
`;

const LoadingCircles = styled(motion.div)`
	height : .5rem;
	width : .5rem;
	border-radius : 50%;
	margin : 0 1rem;
	background : var(--primary);
}
`;

/*
--animation
*/
const LoadingContainerVariants = {
	start:{
		transition :{
      staggerChildren: 0.1
		}
	},
	end:{
		transition :{
			staggerChildren : 0.2
		}
	}
}

const LoadingCirclesVariants = {
	start: {
		y : '0%'
	},
	end : {
		y : "100%"
	}
}

const LoadingCirclesTransition = {
	duration : 0.4,
	yoyo : Infinity,
	ease : "easeInOut"
}

const Loader = props => {
	return(
		<React.Fragment>
		<LoadingContainer>
			<LoadingCircleContainer
				variants={ LoadingContainerVariants }
				initial="start"
				animate="end"
				>
					<LoadingCircles
						variants={ LoadingCirclesVariants }
						transition={ LoadingCirclesTransition }
						initial="start"
						animate="end"
					>
					</LoadingCircles>
					<LoadingCircles
						variants={ LoadingCirclesVariants }
						transition={ LoadingCirclesTransition }
						initial="start"
						animate="end"
					>
					</LoadingCircles>
					<LoadingCircles
						variants={ LoadingCirclesVariants }
						transition={ LoadingCirclesTransition }
						initial="start"
						animate="end"
					>
					</LoadingCircles>
			</LoadingCircleContainer>
		</LoadingContainer>
		</React.Fragment>
	)
}
export default Loader;