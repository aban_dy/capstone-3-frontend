import React,{useState,useRef} from 'react';
import styled from 'styled-components';
import logout from '../assets/icons/logout.png';

const GreetingContainer = styled.div`
	position : absolute;
	right : 1%;
	top : 2%;
	background : rgba(255,255,255,.25);
	background-image: url(${ logout });
	background-size: 16px;
	background-repeat: no-repeat;
	background-position: 95% 50%;
	backdrop-filter : blur(1rem);
	padding: .5rem 2rem .5rem 1rem;
	border-radius : 2rem;
	color: var(--light);
	z-index: 9999;
	transition: all .2s ease-in-out;

	& .logout {
		color: var(--light);
		background : rgba(255,255,255,.25);
		position: absolute;
		top: 110%;
		right: 0%;
		padding: .5rem 4.2rem;
		border-radius: 2rem;
		display: none;
	}

	&:hover {
		cursor : pointer;
		top: 1%;

	}

	& .logout.active {
		display: block;
	}

	& .logout.active:hover {
		background: var(--primary);
	}

`;


const Greetings = ({userEmail}) => {
	const [active ,setActive] = useState(false);
	let logoutBtn = useRef();

	const handleOnClick = (e) => {
		setActive(!active);
		if(active){
			logoutBtn.current.classList.add('active');
		}else{
			logoutBtn.current.classList.remove('active');
		}
	}

	const handleLogout = () => {
		sessionStorage.clear();
		window.location.replace('/login');
	}
	return(
		<React.Fragment>
			<GreetingContainer
				onClick={ handleOnClick }
			>
				{ userEmail }
			<span 
				className="logout" 
				ref={ logoutBtn }
				onClick={ handleLogout }
			>{"logout"}</span>
			</GreetingContainer>
			
		</React.Fragment>
	)
}
export default Greetings;