import React,{ useState } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';


const EditContainer = styled(motion.div)`
	position: fixed;
	top: 50%;
	left: 50%;
	transform: translate(-50%,-50%);
	background : rgba(255,255,255,.25);
	backdrop-filter : blur(1rem);
	border-radius: .2rem;
	color: var(--light);
	width: 400px;
	height: 200px;
	z-index: 9999;

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	padding: 1rem;
`;

const EditInput = styled.input`
	width: 80%;
	border-style: none;
	outline: none;
	background: transparent;
	padding : 1rem;
	border-bottom : 1px solid var(--dark);
	color: var(--light);
	font-size: var(--sm);
	margin: 1rem;

	&:focus{
		border-bottom: 1px solid var(--light);
	}
`;

const SaveButton = styled.button`
	border-style: none;
	outline: none;
	padding : .5rem 1rem;
	border-radius : .2rem;
	background: var(--primary);
	color: var(--light);
	margin-top: 1rem;
	margin-right: 1rem;
`;

const CanceButton = styled.button`
	border-style: none;
	outline: none;
	padding : .5rem 1rem;
	border-radius : .2rem;
	background: var(--dark);
	color: var(--light);
	margin-top: 1rem;
`;

const animation = {
	in:{
		opacity: 1,
	},
	out:{
		opacity: 0,
	}
}

const EditType = ({typeToEdit,setIsEditingType,isEditingType,saveTypeName}) => {

	const [typeName,setTypeName] = useState("");

	const handleOnChange = e => {
		setTypeName(e.target.value)
	}

	const handleSave = () => {
		saveTypeName(typeName);
	}

	return(
		<EditContainer
			animate={ isEditingType ? "in" : "out" }
			initial="out"
			variants={ animation }
		>
			<h1>{"Edit"}</h1>
			<EditInput  
				type="text" 
				defaultValue={ typeToEdit?.name }
				onChange={ handleOnChange }
			/>
			<span>
				<SaveButton
					onClick={ handleSave }
				>{"SAVE"}</SaveButton>
				<CanceButton
					onClick={ () => setIsEditingType(false) }
				>{"CANCEL"}</CanceButton>
			</span>
		</EditContainer>
	)
}

export default EditType;