import styled from 'styled-components';

export const FrostedGlass = styled.div`
	height : 100%;
	width : 100%;
	background : inherit;
	background-attachment : fixed;
	position : relative;

	&:before {
		position: absolute;
		content : "";
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background : rgba(0,0,0,.01);
		backdrop-filter : blur(10px);
	}
`;

