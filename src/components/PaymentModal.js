import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import { SuccessToast, ErrorToast } from './toast/Toast';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';



const ModalContainer = styled.div`
	position: fixed;
	z-index: 9999;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
	height: 100vh;
	width: 100%;
	overflow: hidden;
	background: transparent;
	display: ${ ({isOpen}) => isOpen ? "flex" : "none" };
	align-items: center;
	justify-content: center;
`;
const Modal = styled(motion.div)`
	width: 40%;
	height: 40%;
	background: rgba(255,255,255,.25);
	backdrop-filter: blur(1rem);
	border-radius: .2rem;
	padding: 1rem;
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const ModalHeader = styled.div`
	width: 100%;
	color: var(--light);
	display: flex;
	align-items: center;
	justify-content: space-between;
	border-bottom: 1px solid var(--light);
	padding: .5rem 0px;

	& h1 {
		font-size : var(--sm);
	}

	& strong {
		border: 1px solid var(--info);
		border-radius: .2rem;
		padding: .5rem 1rem;
	}
	& strong:hover {
		background: var(--info);
		cursor: pointer;
	}
`;

const ModalBody = styled.div`
	width: 100%;
	justify-content: space-around;
	padding: 1rem;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const PayButton = styled.div`
	width: 20%;
	display: inline-block;
	outline : none;
	border-style: none;
	border-radius: .2rem;
	padding: .5rem 1rem;
	background: var(--primary);
	color: var(--light);
	text-align: center;
	margin-top: auto;

	&:hover {
		cursor: pointer;
		background: var(--danger);
	}
`;
const Card = styled.div`
	width: 100%;
	padding: 2rem;
	color: var(--success);
	font-size: 1.5rem;
	background: rgba(255,255,255,.5);
	border-radius: .2rem;
`;
//animation settings
const variants = {
	transition: {
		duration: .2,
		type: 'spring',
		stiffness: 200
	},
  open: { opacity: 1, y: 0 },
  closed: { opacity: 0, y: "-100%" },
}

const PaymentModal = ({toggleModal, isOpen, rentId, paid }) => {

	const stripe = useStripe();
	const elements = useElements();

	const saveCharge = async () => {
		const cardElement = elements.getElement(CardElement);
		const { token } = await stripe.createToken(cardElement);

		const apiOptions = {
			method: "POST",
			headers: { "Content-type" : "application/json" },
			body: JSON.stringify({
				token: token.id,
				rentId:  rentId,
				user: sessionStorage.id,
				email: sessionStorage.email,
			})
		}

		let paymentResponse = await fetch("https://capstone-3-backend.herokuapp.com/api/charge",apiOptions);

		if(paymentResponse.ok){
			SuccessToast();
			paid();
		}else{
			ErrorToast();
		}

		toggleModal();
	}

	return(
		<React.Fragment>
			<ModalContainer
				isOpen={ isOpen }
			>
				<Modal
					animate={ isOpen ? "open" : "closed" }
					variants={ variants }
				>
					<ModalHeader>
						<h1>{"PAY VIA STRIPE"}</h1>
						<strong
							onClick={ toggleModal }
						>{"close"}</strong>
					</ModalHeader>
					<ModalBody>
						<Card>
							<CardElement className="card-input" />
						</Card>
					</ModalBody>
					<PayButton onClick={ saveCharge }>{"Submit"}</PayButton>
				</Modal>
			</ModalContainer>
		</React.Fragment>
	)
}
export default PaymentModal;