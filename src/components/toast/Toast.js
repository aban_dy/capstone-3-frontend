import React from 'react';
import { toast } from 'react-toastify';
import './Toast.css';
// import success from '../../assets/icons/check.svg';
// import error from '../../assets/icons/error.svg';
// import info from '../../assets/icons/info.svg';
// import warning from '../../assets/icons/warning.svg';




const Toast = () => {
	return(<React.Fragment></React.Fragment>)
}

export const Status = (message) => {

	toast(message,{
  className: 'black-background',
  bodyClassName: "grow-font-size",
  progressClassName: 'fancy-progress-bar'
	});

}

export const SuccessToast = () => {

	toast("Success",{
  className: 'black-background success',
  bodyClassName: "grow-font-size",
  progressClassName: 'fancy-progress-bar'
	});

}

export const ErrorToast = () => {

	toast("Error",{
  className: 'black-background error',
  bodyClassName: "grow-font-size",
  progressClassName: 'fancy-progress-bar'
	});

}

export default Toast;