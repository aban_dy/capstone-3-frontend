import React from 'react';
import {
	CategoryRow,
	CategoryDelete,
	CategoryUpdate,
	Buttons,
}from '../DashboardStyled';
const Type = ({type,deleteType,editType}) => {
	return(
		<CategoryRow>

			<p>
			{ type.name }
			</p>

		<Buttons>
			<CategoryDelete
				onClick={ ()=> deleteType(type._id) }
			/>

			<CategoryUpdate
				onClick={ ()=> editType(type) }
			/>
		</Buttons>

		</CategoryRow>
		)
}
export default Type;