import React,{ useState,useEffect } from 'react';
import SideNav from '../../components/SideNav';
import { useHistory } from 'react-router-dom';
import { SuccessToast,ErrorToast } from '../../components/toast/Toast';
import {
	DashboardContainer,
} from './DashboardStyled';

//import componets
import AdminGrid from './grid/AdminGrid';
import EditModel from '../../components/EditModel';
import EditType from '../../components/EditType';
import AdminCar from './car/AdminCar';
import UserCar from './car/UserCar';
//fetch variables
const headers = { "Content-type" : "application/json" };
const URL = "https://capstone-3-backend.herokuapp.com/api";

const Dashboard = props => {
/*----------------*/
/*-----STATES-----*/
/*----------------*/

/*-Model States-*/
const [models,setModels] = useState([]);
const [modelName, setModelName] = useState("");
const [modelToEdit, setModelToEdit] = useState({});
const [ isEditingModel, setIsEditingModel ] = useState(false);
/*-Type States-*/
const [types, setTypes] = useState([]);
const [typeName,setTypeName] = useState("");
const [typeToEdit, setTypeToEdit] = useState({});
const [ isEditingType, setIsEditingType ] = useState(false);

let history = useHistory();


/*USER STATES*/
const [isAdmin,setIsAdmin] = useState(false);

/*-USE-EFFECT-*/
useEffect(()=>{

if(sessionStorage.token){
	if(sessionStorage.isAdmin === "true"){
	setIsAdmin(true);
	}
}else{
	history.push('/login');
}

fetchModels();
fetchTypes();

},[sessionStorage]);

/*-------------------*/
/*-----FUNCTIONS-----*/
/*-------------------*/


const fetchModels = () => {
	//fetch all models
	fetch(`${URL}/models`)
	.then(res=>res.json())
	.then(res=>{
		setModels(res);
	});
}
const fetchTypes = () => {
	//fetch all types
	fetch(`${URL}/types`)
	.then(res=>res.json())
	.then(res=>{
		setTypes(res);
	})
}
const modelOnChange = e => {
	setModelName(e.target.value);
}
const saveModelName = async (newModel) => {
	if(isEditingModel){
		let editedModelName = newModel;		
		if(newModel === "") editedModelName = modelToEdit.name;

		let res = await fetch(`${ URL }/updatemodel`,{
			method: "PATCH",
			headers : headers,
			body: JSON.stringify({
				id: modelToEdit._id,
				name : editedModelName
			})
		});

		if(res.status === 200){

			let newModels = models.map(model=>{
				if( modelToEdit._id === model._id ){
					 model.name = editedModelName
				}

				return model
			})

			setModels(newModels);
			setIsEditingModel(false);
			SuccessToast();
		}else{
			ErrorToast();
		}

	}else{

		if(modelName !== ""){
			let res = await fetch(`${ URL }/addmodel`,{
				method: "POST",
				headers: headers,
				body: JSON.stringify({ name: modelName })
			});
			if(res.status === 200){
				let result = await res.json();
				let newModels = [result,...models];
				setModels(newModels);
				setModelName("");
				SuccessToast();
			}else{
				ErrorToast();
			}
		}else{
			ErrorToast();
		}

	}
}
const deleteModel = async (id) => {
	let res = await fetch(`${URL}/deletemodel`,{
		method: "DELETE",
		headers: headers,
		body: JSON.stringify({ id })
	});

	if(res.status === 200){
		let newModels = models.filter(model =>{
			return model._id !== id
		})
		setModels(newModels);
		SuccessToast();
	}else{
		ErrorToast();
	}
}
const editModel = (model) => {
	setIsEditingModel(true);
	setModelToEdit(model);
}
const typeOnChange = e => {
	setTypeName(e.target.value);
}
const saveTypeName = async (newType) => {
	if(isEditingType){
		let editedTypeName = newType;		
		if(newType === "") editedTypeName = typeToEdit.name;

		let res = await fetch(`${ URL }/updatetype`,{
			method: "PATCH",
			headers : headers,
			body: JSON.stringify({
				id: typeToEdit._id,
				name : editedTypeName
			})
		});

		if(res.status === 200){

			let newTypes = types.map(type=>{
				if( typeToEdit._id === type._id ){
					 type.name = editedTypeName
				}

				return type
			})

			setTypes(newTypes);
			setIsEditingType(false);
			SuccessToast();
		}else{
			ErrorToast();
		}

	}else{

		if(typeName !== ""){
			let res = await fetch(`${ URL }/addtype`,{
				method: "POST",
				headers: headers,
				body: JSON.stringify({ name: typeName })
			});
			if(res.status === 200){
				let result = await res.json();
				let newTypes = [result,...types];
				setTypes(newTypes);
				setTypeName("");
				SuccessToast();
			}else{
				ErrorToast();
			}
		}else{
			ErrorToast();
		}

	}
}
const editType = (type) => {
	setIsEditingType(true);
	setTypeToEdit(type);
}
const deleteType = async (id) => {
	let res = await fetch(`${URL}/deletetype`,{
		method: "DELETE",
		headers: headers,
		body: JSON.stringify({ id })
	});

	if(res.status === 200){
		let newTypes = types.filter(type =>{
			return type._id !== id
		})
		setTypes(newTypes);
		SuccessToast();
	}else{
		ErrorToast();
	}
}

return(
	<React.Fragment>

		<SideNav />
		{isEditingModel && isAdmin &&
			<EditModel 
				modelToEdit={ modelToEdit } 
				setIsEditingModel={setIsEditingModel}
				isEditingModel={ isEditingModel }
				saveModelName={ saveModelName }
		/>}

		{isEditingType && isAdmin &&
			<EditType
				typeToEdit={ typeToEdit } 
				setIsEditingType={setIsEditingType}
				isEditingType={ isEditingType }
				saveTypeName={ saveTypeName }
		/>}

		{ isAdmin ? 
			(
		<DashboardContainer>
			<AdminGrid
				models={ models }
				modelOnChange={ modelOnChange }
				saveModelName={ saveModelName }
				deleteModel={ deleteModel }
				editModel={ editModel }
				modelToEdit={ modelToEdit }
				types={ types }
				typeOnChange={ typeOnChange }
				saveTypeName={ saveTypeName }
				editType={ editType }
				deleteType={ deleteType }
				modelName={ modelName }
				typeName={ typeName }
			 />
			<AdminCar 
				types={types}
				models={models}
			/>
		</DashboardContainer>
			) 

		: (
				<DashboardContainer>
					<UserCar />
				</DashboardContainer>
			) 
	}
	</React.Fragment>
	)
}
export default Dashboard;