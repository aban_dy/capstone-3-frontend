import React from 'react';
import styled from 'styled-components';
import logo from '../../../assets/icons/orange-logo.svg';
import moment from 'moment';


const Container = styled.div`
	height : 400px;
	width : 800px;
	margin: 2rem auto;
	padding : 1rem;
	background : var(--info);
	color: var(--light);

	& h1 {
		color: var(--light);
		margin: 1rem 0px;
	}
	& code {
		margin-top: 1rem;
		padding : 1rem;
		letter-spacing: 1rem;
		text-align: center
		text-transform: uppercase;
	}
`;

const ImageContainer = styled.div`
	width : 100%;
	height: 100px;
	display: flex;
	justify-content: space-between;
	align-items: center;

	& label {
		font-size : 1.8rem;
		letter-spacing: 1rem;
	}
`;

const BillDetails = styled.div`
	width: 100%;
	margin-top: 1rem;
	display: flex;
	align-items: center;
	justify-content: space-between;
`;
	
const BillTo = styled.div`
	
`;

const BillDate = styled.div`
	
`;

const BillNumer = styled.div`

`;

const BillBody = styled.div`
	width: 100%;
	margin-top: 1rem;
	padding: .5rem;
	& table {
		width: 100%;
		text-align: left;
		margin-bottom: 1rem;
		
	}
	& thead {
		border-top : .5px dashed var(--success);
		border-bottom : .5px dashed var(--success);
	}

	& th {
		padding: 1rem 0px;
	}

	& td {
		padding: 1rem 0px;
	}

`;

class ComponentToPrint extends React.Component {
	render() {

		return(
		<Container>
			<ImageContainer>
				<img src={ logo } alt="logp" />
				<label>{"RECEIPT"}</label>
			</ImageContainer>
			<BillDetails>
				<BillTo>
					<h1>{"BILLED TO "}</h1>
					<h1>{ sessionStorage.email ? sessionStorage.email : "user@user.com" }</h1>
				</BillTo>
				<BillNumer>
					<h1>{"RECEIPT NO"}</h1>
					<h1>{"#"}{ moment(this.props.rent?.createdAt).format('x') }</h1>
				</BillNumer>
				<BillDate>
					<h1>{"DATE "}</h1>
					<h1>{ moment(this.props.rent?.createdAt).format('ll') }</h1>
				</BillDate>
			</BillDetails>
			<BillBody>
				<table>
					<thead>
						<th>Car Details</th>
						<th>Amount</th>
						<th>Payment Status</th>
						<th>Date</th>
					</thead>
					<tbody>
						<tr>
							<td>{ this.props.rent?.car?.name }</td>
							<td>{ this.props.rent?.amount }</td>
							<td>{ this.props.rent?.payment }</td>
							<td>
								{ moment(this.props.rent.date.from).format('ll') }
								{"-"}
								{ moment(this.props.rent.date.from).format('ll') }
							</td>
						</tr>
					</tbody>
				</table>
			</BillBody>
			<code>{"this is non-refundable"}</code>
		</Container>
	)
	}
}

export default ComponentToPrint