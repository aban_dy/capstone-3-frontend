import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

const LoaderContainer =styled(motion.div)`
	width: 100%;
	display: flex;
	padding: 1rem;
	background: var(--info);
	box-shadow: 4px 4px 12px rgba(0,0,0,.8);
	border-radius: .2rem;
	align-items: center;
	justify-content: space-between;
	margin : 1rem 0px;
	height: 64px;
`;

const LoadingCirclesVariants = {
	start: {
		y : '0%',
		opacity : 1
	},
	end : {
		y : "10%",
		opacity: 0
	}
}

const LoadingCirclesTransition = {
	duration : 0.4,
	yoyo : Infinity,
	ease : "easeInOut"
}

const SrollLoader = props => {
	return(
		<LoaderContainer 
			variants={ LoadingCirclesVariants }
			transition={ LoadingCirclesTransition }
			initial="start"
			animate="end"
		>
			
		</LoaderContainer>
	)
} 

export default SrollLoader;