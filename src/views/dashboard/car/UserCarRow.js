import React,{ useRef } from 'react';
import { Car,CarImage,CarText,CarActions } from '../DashboardStyled';
import moment from 'moment';
import ReactToPrint from 'react-to-print';
import ComponentToPrint from './ComponentToPrint';

const URL = "https://capstone-3-backend.herokuapp.com/";

const UserCarRow = ({rent,toggleModal,deleteRent}) => {
	let ComponentToPrintRef = useRef();
	return(
	<React.Fragment>
		<Car>
			<CarImage src={ `${ URL }${ rent?.car?.image }` } />
			<CarText>
				<p>{ rent?.car?.name }</p>
				<p>{"Total: "}{ rent?.amount }{" php"}</p>
				<p>{"Status: "}{ rent?.payment }</p>
				<p>{"Date: "}<br/>{ moment(rent.date.from).format('ll') }<br/>{ moment(rent.date.to).format('ll') }</p>
			</CarText>
			<CarActions>
				<button
					onClick={ () => deleteRent(rent._id) }
					disabled={ rent?.payment === "Pending" ? false : true }
				>{"DELETE"}</button>
				<button
					onClick={ ()=> toggleModal( rent._id ) }
					disabled={ rent?.payment !== "Pending" ? true : false }
				>{"PAY"}</button>
				<span>
					<ReactToPrint 
						trigger={ () => <button disabled={ rent?.payment === "Pending" ? true : false }>{"PRINT"}</button> }
						content={ () => ComponentToPrintRef.current }
					/>
				</span>
			</CarActions>
			<span style={{display: "none"}}> 
				<ComponentToPrint ref={ComponentToPrintRef} rent={ rent } />
			</span>
		</Car>
	</React.Fragment>
	)
}
export default UserCarRow;