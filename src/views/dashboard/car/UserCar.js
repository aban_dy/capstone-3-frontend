import React,{ useEffect,useState } from 'react';
import PaymentModal from '../../../components/PaymentModal';
import { SuccessToast, ErrorToast } from '../../../components/toast/Toast';
import { 
	CarTable, 
	CarGrid, 
	CarGridTitle,
	AddCarButton
	} from '../DashboardStyled';
import UserCarRow from './UserCarRow';
import InfiniteScroll from 'react-infinite-scroller';
import SrollLoader from './Loader';

/*STRIPE*/
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
const stripePromise = loadStripe("pk_test_k3ShE7JQ6y55tQ1N9KcJeVMY00Rewwqy5h");


const URL = "https://capstone-3-backend.herokuapp.com/api";
const headers = { 'Content-type' : 'application/json' };

const pageVariants = {
	in:{
		opacity : 1,
		y: "0"
	},
	out:{
		opacity : 0,
		y: "-100vh"
	},
	init:{
		y: "100vh"
	}
}

const pageTransition = {
	duration : .4,
	type : "spring",
	stiffness : 100
}

const UserCar = props => {
/*----------------*/
/*-----STATES-----*/
/*----------------*/
const [rents, setRents] = useState([]);
const [hasMore,setHasMore] = useState(false);
const [ isModalOpen, setIsModalOpen ] = useState(false);
const [rentId, setRentId] = useState("");
const [ paymentDone, setPaymentDone ] = useState(false);

useEffect(()=>{
	//fetcht rents
	fetchRent();
	
},[paymentDone]);

const fetchRent = async () => {
	let res = await fetch(`${URL}/myrents/${ sessionStorage.id }`);
	let result = await res.json();
	setRents(result);
	if(result.length > 10){
		setHasMore(true);
	}
}

/*-------------------*/
/*-----FUNCTIONS-----*/
/*-------------------*/
const toggleModal = (rentId) => {
	setIsModalOpen(!isModalOpen);
	setRentId(rentId)
}

const paid = () => {
	setPaymentDone(!paymentDone);
}

const deleteRent = async (id) => {
	let res = await fetch(`${ URL }/deleterent`,{
		method: 'DELETE',
		headers: headers,
		body: JSON.stringify({ id })
	});
	if(res.status === 200){
		let result = await res.json();
		let newRents = rents.filter(rent => {
			return id !== rent._id
		})
		setRents(newRents);
		SuccessToast();
	}else{
		ErrorToast();
	}
	
}

	return(
		<React.Fragment>

			<Elements stripe={ stripePromise }>
				<PaymentModal 
					toggleModal={ toggleModal } 
					isOpen={ isModalOpen }
					rentId={ rentId }
					paid={ paid }
					/>
			</Elements>

			<CarTable
				animate="in"
				initial="init"
				exit="out"
				variants={ pageVariants }
				transition={ pageTransition }

			>

				<CarGrid>
					<CarGridTitle>{"CARS"}</CarGridTitle>
					<strong>{"NOTE: all pending payments will be deleted after 5 minutes."}</strong>
					<InfiniteScroll
						loadMore={ fetchRent }
						hasMore={ hasMore }
						pageStart={ 0 }
						loader={ <SrollLoader key={ 0 } /> }
					>
					{!rents ?  <p>{"No cars to show"}</p> : rents.map(rent=>(
						<UserCarRow
							key={ rent._id } 
							rent={ rent }
							toggleModal={ toggleModal }
							deleteRent = { deleteRent }
						/>
					))}

					</InfiniteScroll>
				</CarGrid>
			</CarTable>
		</React.Fragment>
	)
}
export default UserCar;