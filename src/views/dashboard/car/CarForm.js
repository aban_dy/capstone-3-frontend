import React,{ useState , useRef,useEffect} from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import upload from '../../../assets/icons/upload.png';
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
} from 'styled-dropdown-component';


const AddContainer = styled(motion.div)`
	position: fixed;
	top: 50%;
	left: 50%;
	transform: translate(-50%,-50%);
	background : rgba(255,255,255,.25);
	backdrop-filter : blur(1rem);
	border-radius: .2rem;
	color: var(--light);
	width: 400px;
	height: auto;
	z-index: 9999;

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	padding: 1rem;
`;

const InputGroup = styled.div`
	width: 100%;
	padding: 1rem;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items:center;

	& .image {
		width: 80%;
	}

	& p {
		margin-top : 1rem;
	}
	& .image-preview {
		border-radius: .2rem;
		object-fit: cover;
	}
`;

const Group =styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	align-items: center;
`;

const AddInput = styled.input`
	width: 80%;
	font-size : 1.2rem;
	border-style: none;
	outline: none;
	background: transparent;
	border-bottom : 1px solid var(--dark);
	color: var(--light);
	margin: 1rem;

	&::-webkit-outer-spin-button,
	&::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
	}

	&:focus{
		border-bottom: 1px solid var(--light);
	}
`;

const ImageInput = styled.input`
	width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
`;
const ImageLabel = styled.label`
  font-weight: 700;
  color: white;
  width: 53%;
  background-color: var(--info);
  display: inline-block;
  padding : 1rem 2rem;
  border-radius: .2rem;
  text-align: center;
  position: relative;

  &:after {
  	content: "";
  	position: absolute;
  	width: 100%;
  	top: 0;
  	left: 0;
  	bottom: 0;
  	right: 0;
  	z-index: 1;
  	background: transparent;
  	background-image: url(${ upload });
  	background-position: 95% 50%;
  	background-repeat : no-repeat;
  	background-size : 24px;
  }

  &:hover{
  	color: var(--primary);
  	cursor: pointer;
  }
`;

const SaveButton = styled.button`
	border-style: none;
	outline: none;
	padding : .5rem 1rem;
	border-radius : .2rem;
	background: var(--primary);
	color: var(--light);
	margin-top: 1rem;
	margin-right: 1rem;

	&:hover {
		transform : scale(1.1);
	}
`;

const CanceButton = styled.button`
	border-style: none;
	outline: none;
	padding : .5rem 1rem;
	border-radius : .2rem;
	background: var(--dark);
	color: var(--light);
	margin-top: 1rem;

	&:hover {
		transform : scale(1.1);
	}
`;

const Button = styled.span`
	border-style: none;
	outline: none;
	border-radius: .2rem;
	display: inline-block;
	width : 212px;
	text-align: center;
	padding: 1rem 2rem;
	background: var(--info);
	color: var(--light);
	margin-top: 1rem;

	&:hover {
		background: var(--primary);
		cursor: pointer;
	}
`;

const animation = {
	in:{
		opacity: 1,
	},
	out:{
		opacity: 0,
	}
}

const CarForm = ({types,models,saveCar,toggleCarForm,carToEdit,isEditing}) => {

//input states
const[name,setName] = useState("");
const[type,setType] = useState({});
const[model,setModel] = useState({});
const[price,setPrice] = useState(null);
const[image,setImage] = useState(null);
const [typeHidden, setTypeHidden] = useState(true);
const [modelHidden, setModelHidden] = useState(true);
const[imageName,setImageName] = useState("");

let fileInput = useRef();

	useEffect(()=>{
		if(isEditing){
			setType(carToEdit.type);
			setModel(carToEdit.model);
		}
	},[isEditing,carToEdit.type,carToEdit.model]);

	const handleSave = async () => {
		let imageUrl = await uploadMainImage(image);

		let newCar = {
			name : name,
			price: price,
			type: type._id,
			model: model._id,
			image : imageUrl?.imageUrl ? imageUrl.imageUrl : ""
		}

		saveCar(newCar);
		toggleCarForm();
	}

	const imagePreview = (e) => {

		if(e.target.files.length !== 0){

			let file = e.target.files[0];
			setImageName(file.name.toLowerCase());

			if(file.name.toLowerCase().match(/\.(jpg||jpeg||png||gif)$/)){
				setImage(file);
			}

		}else{
			setImage("")
		}

	}

	const uploadMainImage = async (imageToSave) => {
		if(image){
			let data = new FormData();
			data.append('image',image,imageToSave.name.toLowerCase());

			const imageData = await fetch("https://capstone-3-backend.herokuapp.com/upload",{
				method : "POST",
				body : data
			})

			const imageUrl = await imageData.json();

			return imageUrl;
			}
	}

	return(
		<AddContainer
			animate="in"
			initial="out"
			variants={ animation }
		>
			<p>{"ADD CAR"}</p>
			<Group>

				<InputGroup>
					<label>{"name"}</label>
					<AddInput 
						type="text" 
						placeholder="enter name..." 
						onChange={ (e)=>setName(e.target.value) }
						defaultValue={ carToEdit?.name ? carToEdit.name : "" }
					/>
				</InputGroup>
				<InputGroup>
					<label>{"price"}</label>
					<AddInput 
						type="number" 
						onChange={ (e)=>setPrice(e.target.value) }
						defaultValue={ carToEdit?.price ? carToEdit.price : "" }
					/>
				</InputGroup>

					<Dropdown>
			      <Button dropdownToggle onClick={() => setModelHidden(!modelHidden)}>
			        {	model?.name ? model.name : "choose model" }
			      </Button>
			      <DropdownMenu hidden={modelHidden} toggle={() => setModelHidden(!modelHidden)}>
			        	{models.map(model=>(
			        	<DropdownItem 
			        		key={ model._id }
			        		onClick={()=>{
			        			setModel(model);
			        			setModelHidden(!modelHidden);
			        		}} 
			        	>{ model.name }</DropdownItem>
			        ))}
			      </DropdownMenu>
			    </Dropdown>

			    <Dropdown>
			      <Button dropdownToggle onClick={() => setTypeHidden(!typeHidden)}>
			        {	type?.name ? type.name : "choose type"  }
			      </Button>
			      <DropdownMenu hidden={typeHidden} toggle={() => setTypeHidden(!typeHidden)}>
			        {types.map(type=>(
			        	<DropdownItem 
			        		key={ type._id }
			        		onClick={()=>{
			        			setType(type);
			        			setTypeHidden(!typeHidden);
			        			}} 
			        	>{type.name}</DropdownItem>
			        	))}
			      </DropdownMenu>
			    </Dropdown>

				<InputGroup>
					<ImageLabel
						onClick={ ()=> fileInput.click() } 
					>{"choose image"}</ImageLabel>
					<ImageInput type="file" ref={ ref => fileInput = ref } onChange={ imagePreview }/>
					<p>{imageName}</p>
				</InputGroup>

			</Group>

			<span>
				<SaveButton
					onClick={ handleSave }
				>{"SAVE"}</SaveButton>
				<CanceButton
					onClick={ toggleCarForm }
				>{"CANCEL"}</CanceButton>
			</span>

		</AddContainer>
	)
}

export default CarForm;