import React,{ useEffect,useState } from 'react';
import { SuccessToast, ErrorToast } from '../../../components/toast/Toast';
import { 
	CarTable, 
	CarGrid, 
	CarGridTitle,
	AddCarButton
	} from '../DashboardStyled';
import InfiniteScroll from 'react-infinite-scroller';
import CarForm from './CarForm';
import CarRow from './CarRow';
import SrollLoader from './Loader';




const pageVariants = {
	in:{
		opacity : 1,
		y: "0"
	},
	out:{
		opacity : 0,
		y: "-100vh"
	},
	init:{
		y: "100vh"
	}
}

const pageTransition = {
	duration : .4,
	type : "spring",
	stiffness : 100
}



const URL = "https://capstone-3-backend.herokuapp.com/api";
const headers = { 'Content-type' : 'application/json' };

const AdminCar = ({types,models}) => {
/*---------------*/
/*----STATES-----*/
/*---------------*/
const [cars,setCars] = useState([]);
const [isEditing,setIsEditing] = useState(false);
const [carToEdit,setCarToEdit] = useState({});
const [isCarFormOpen, setIsCarFormOpen] = useState(false);
const [addDone, setAddDone] = useState(false);
const [hasMore, setHasMore] = useState(false);
// const [itemsLength, setItemsLength] = useState(0);


/*----------------*/
/*---USE-EFFECT---*/
/*----------------*/
useEffect(()=>{

	fetchCars();

},[addDone]);

/*---------------*/
/*---FUNCTIONS---*/
/*---------------*/


const fetchCars = async () => {
	let res = await fetch(`${URL}/cars`)
	let result = await res.json();

	if(result.length > 10){
		// setItemsLength(result.length);
		setHasMore(true);
	}

	setCars(result);
}
const toggleCarForm = () => {
	setIsCarFormOpen(!isCarFormOpen);
}

const editHandler = (car) => {
	setIsEditing(true);
	setCarToEdit(car);
	setIsCarFormOpen(true);
}

const saveCar = async (car) => {

	if(isEditing){
		let editedName = car.name;
		let editedPrice = car.price;
		let editedType = car.type;
		let editedModel = car.model;
		let editedImage = car.image;

		if(car.name === "") editedName = carToEdit.name;
		if(car.price === null) editedPrice = carToEdit.price;
		if(car.type === undefined) editedType = carToEdit.type._id;
		if(car.model === undefined) editedModel = carToEdit.model._id;
		if(car.image === "") editedImage = carToEdit.image;

		let res = await fetch(`${ URL }/updatecar`,{
			method: "PATCH",
			headers: headers,
			body: JSON.stringify({
				id: carToEdit._id,
				name : editedName,
				price: editedPrice,
				type: editedType,
				model: editedModel,
				image : editedImage
			})
		})

		if(res.status === 200) {
			SuccessToast();
		}else{
			ErrorToast();
		}

	}else{
		let res = await fetch(`${ URL }/addcar`,{
			method: "POST",
			headers: headers,
			body : JSON.stringify({
				name : car.name,
				image: car.image,
				type: car.type,
				model: car.model,
				price: car.price
			})
		});

		if(res.status === 200){
			SuccessToast();
		}else{
			ErrorToast();
		}
	}
	setAddDone(!addDone) // just to rerun the fetch to
}


const deleteCar = async (id) => {
	let res = await fetch(`${ URL }/deletecar`,{
		method: "DELETE",
		headers: headers,
		body: JSON.stringify({id})
	});
	if(res.status === 200){
		SuccessToast();
	}else{
		ErrorToast();
	}
	setAddDone(!addDone);
}

return(
<React.Fragment>
<CarTable
	animate="in"
	initial="init"
	exit="out"
	variants={ pageVariants }
	transition={ pageTransition }

>

	<CarGrid>
		<AddCarButton onClick={toggleCarForm}/>
		<CarGridTitle>{"CARS"}</CarGridTitle>
		<InfiniteScroll
			loadMore={ fetchCars }
			hasMore={ hasMore }
			pageStart={ 0 }
			loader={ <SrollLoader key={ 0 } /> }
		>
			{cars.map(car=>(
				<CarRow 
					key={ car._id }  
					car={ car }
					deleteCar={ deleteCar }
					editHandler={ editHandler }
				/>
			))}
		</InfiniteScroll>
	</CarGrid>
</CarTable>
{ isCarFormOpen &&
	<CarForm 
		types={ types }
		models={ models }
		saveCar={ saveCar }
		toggleCarForm={ toggleCarForm }
		carToEdit={ carToEdit }
		isEditing={ isEditing } 


	/> 
}
</React.Fragment>
)
}
export default AdminCar