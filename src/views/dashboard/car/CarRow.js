import React from 'react';
import { Car,CarImage,CarText,CarActions } from '../DashboardStyled'; 


const URL = "https://capstone-3-backend.herokuapp.com/"


const CarRow = ({car,deleteCar,editHandler}) => {
	return(
		<Car>
			<CarImage src={ `${ URL }${ car.image }` } />
			<CarText>
				<p>
					{car.name}
				</p>
				<p>
					{"Php "}{ car.price }
				</p>
				<p>
					{ car.model.name }
				</p>
				<p>
					{ car.type.name }
				</p>
			</CarText>
			<CarActions>
				<button
					onClick={ ()=> editHandler(car) }
				>{"EDIT"}</button>
				<button
					onClick={ () => deleteCar(car._id) }
				>{"DELETE"}</button>
			</CarActions>
		</Car>
	)
}
export default CarRow;