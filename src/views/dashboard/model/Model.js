import React from 'react';
import {
	CategoryRow,
	CategoryDelete,
	CategoryUpdate,
	Buttons
}from '../DashboardStyled';

const Model = ({model,deleteModel,modelToEdit,editModel,isEditing}) => {

	return(
		<CategoryRow>
			<p>
			{ model.name }
			</p>
			<Buttons>
			<CategoryDelete
				onClick={ ()=> deleteModel(model._id) }
			/>
			<CategoryUpdate
				onClick={ ()=> editModel(model) }
			/>
			</Buttons>
		</CategoryRow>
	)
}
export default Model;