import React,{ useState } from 'react';
import Model from '../model/Model';
import Type from '../type/Type';
import DayPicker from 'react-day-picker';
import moment from 'moment';
import 'react-day-picker/lib/style.css';
import { CSVLink } from "react-csv";
import { 
	Grid,
	Buttons,
	CategoryRow,
	CategoryDownload,
	Col1,
	Col2,
	Col3,
	Categories,
	TitleCard,
	AddButton,
	AddInput,
	AddModel
	} from '../DashboardStyled';
const pageVariants = {
	in:{
		opacity : 1,
		y: "0"
	},
	out:{
		opacity : 0,
		y: "-100vh"
	},
	init:{
		y: "100vh"
	}
}
const pageTransition = {
	duration : .4,
	type : "spring",
	stiffness : 100
}
const AdminGrid = ({
models,
deleteModel,
editModel,
modelToEdit,
modelOnChange,
modelName,
saveModelName,
types,
deleteType,
editType,
typeToEdit,
typeOnChange,
typeName,
saveTypeName,
}) => {

const [date,setDate] = useState(null);
const [rents, setRents] = useState([]);

const handleDayClick = async (day) => {
	
	let res = await fetch('http://localhost:4000/api/reports',{
		method: "POST",
		headers : {"Content-type" : "application/json"},
		body: JSON.stringify({
			date : day
		})
	});
	if(res.status === 200){
		let result = await res.json();
		setDate(day);
		setRents(result);
	}
}

let data = [
  ["Name", "amount", "email"]
];

if(rents){
	rents.forEach(rent => {
		let dataRow = [];
		dataRow.push(rent.car.name);
		dataRow.push(rent.amount);
		dataRow.push(rent.user.email);
		data.push(dataRow);
	});
}



return(
<Grid>
		<Col1
			animate="in"
			initial="init"
			exit="out"
			variants={ pageVariants }
			transition={ pageTransition }
		>
			<Categories>
				<TitleCard>{"Models"}</TitleCard>
					{models.map(model=>
					(<Model 
						key={ model._id } 
						model={ model } 
						deleteModel={ deleteModel }
						editModel={ editModel } 
						modelToEdit={ modelToEdit }
						/>))}
			</Categories>

			<AddModel>
				<h1>{"Add model"}</h1>
				<AddInput type="text" onChange={ modelOnChange } value={ modelName }/>
				<AddButton onClick={ saveModelName }>{"ADD"}</AddButton>
			</AddModel>
	</Col1>

	<Col2
		animate="in"
		initial="init"
		exit="out"
		variants={ pageVariants }
		transition={ pageTransition }
	>
		<Categories color={"var(--info)"}>
			<TitleCard>{"Types"}</TitleCard>
			{types.map(type => (
			<Type 
				key={ type._id }
				type={ type }
				deleteType={ deleteType }
				editType={ editType }
				typeToEdit={ typeToEdit }
			/>))}
		</Categories>

		<AddModel color={"var(--info)"}>
			<h1>{"Add type"}</h1>
			<AddInput 
				type="text"
				onChange={ typeOnChange }
				value={ typeName }
			/>
			<AddButton onClick={ saveTypeName }>{"ADD"}</AddButton>
		</AddModel>
	</Col2>

	<Col3
		animate="in"
		initial="init"
		exit="out"
		variants={ pageVariants }
		transition={ pageTransition }
	>
		<Categories color={"var(--danger)"}>
			<TitleCard>{"Reports"}</TitleCard>
			<CategoryRow>
			<p>{ date ? moment(date).format('ll') : "Choose date"}</p>
			<Buttons>
			<CSVLink
				data={ data }
				target="_blank"
				filename={ `${moment(date).format('ll')}-reports.csv` }
			>
				<CategoryDownload disabled={ rents.length !== 0 ? false : true }/>
			</CSVLink>
			</Buttons>
			</CategoryRow>
			<DayPicker 
					className="Selectable"
					numberOfMonths = { 1 }
					fromMonth = { new Date() }
					selectedDays = { date }
					onDayClick = { handleDayClick }
				/>
		</Categories>
	</Col3>
</Grid>
	)
}
export default AdminGrid;