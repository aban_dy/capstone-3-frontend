import styled from 'styled-components';
import { motion } from 'framer-motion';
import deleteImg from '../../assets/icons/delete.png';
import editImg from '../../assets/icons/edit.png';
import add from '../../assets/icons/add.png';



export const DashboardContainer = styled.div`
	width: 100%;
	min-height: 100vh;
	background: var(--dark);
	height: auto;
	overflow : hidden;
	display: flex;
	flex-direction: column;
	align-items: center;
`;

export const Grid = styled.div`
	width: 100%;
	display: grid;
	grid-template-columns : repeat(3,1fr);
	overflow-y: hidden;
`;

//column1
export const Col1 = styled(motion.div)`
	width : 100%;
	background: var(--dark);
	display : flex;
	flex-direction: column;
	align-items: center;
	padding: 2rem 0px;
`;


export const Categories = styled.div`
	width : 80%;
	background: ${ ({color}) => color ? color : "rgba(255,255,255,.25)" };
	border-radius : .2rem;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	padding: 2rem 1rem;
	margin-top: 2rem;
	color: var(--dark);
	position: relative;

	& h1 {
		color: var(--light);
		font-size : var(--sm);
	}
	.Selectable {
		background: var(--info);
		color: var(--dark);
	}
`;


export const TitleCard = styled.div`
	position: absolute;
	top: -32px;
	right: 5%;
	left: 5%;
	background: var(--light);
	border-radius: .2rem;
	box-shadow: 2px 2px 14px rgba(0,0,0,.8);
	color: var(--dark);
	text-transform : uppercase;
	font-weight: 400;
	display: flex;
	justify-content: center;
	align-items: center;
	min-height: 64px;
`;

export const CategoryRow = styled.div`
	border-top: 1px solid rgba(0,0,0,.5);
	color: var(--success);
	padding: .5rem 1rem;
	width : 80%;
	margin-top : 1rem;
	display: flex;
	justify-content: space-between;
	align-items: center;

	& p {
		color: var(--light);
	}
`;

export const CategoryDelete = styled.button`
	border-style: none;
	background: var(--light);
	background-image: url(${ deleteImg });
	background-size: 24px;
	background-repeat: no-repeat;
	background-position: center;
	border-radius: 50%;
	color: var(--light);
	height: 32px;
	margin-right: .5rem;
	outline: none;
	padding: .5rem 1rem;
	width: 32px;
	transition: all .2s ease-in-out;

	&:hover {
		transform : scale(1.2);
	}
`;

export const CategoryUpdate = styled.button`
	border-style: none;
	background-image: url(${ editImg });
	background-size: 24px;
	background-repeat: no-repeat;
	background-position: center;
	border-radius: 50%;
	color: var(--light);
	height: 32px;
	outline: none;
	padding: .5rem 1rem;
	width: 32px;
	transition: all .2s ease-in-out;

	&:hover{
		transform: scale(1.2);
	}
`;

export const Buttons = styled.div`
`;

export const AddModel = styled.div`
	width : 80%;
	background:  ${ ({color}) => color ? color : "rgba(255,255,255,.25)" };
	border-radius : .2rem;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	padding: 2rem 1rem;
	margin-top: 1rem;
	color: var(--dark);

	& h1 {
		color: var(--light);
		font-size : var(--sm);
	}
`;

export const AddInput = styled.input`
	outline: none;
	border-style: none;
	background: transparent;
	background-image: url(${ editImg });
	background-position: 98% 50%;
	background-size : 16px;
	background-repeat: no-repeat;
	border-radius: .2rem;
	border-bottom: 1px solid var(--light);
	color: var(--light);
	padding: .5rem 1rem;
	margin-top: 1rem;
	width: 80%;
`;

export const AddButton = styled.button`
	border-style: none;
	background: var(--success);
	border-radius: .2rem;
	outline: none;
	width: 90%;
	display: block;
	padding: .5rem 2rem;
	margin-top: 1rem;
	color: var(--light);
	transition: all .2s ease-in-out;

	&: hover {
		background : var(--dark);
	}
`;

export const Col2 = styled(motion.div)`
	width : 100%;
	height: auto;
	background: var(--dark);
	display : flex;
	flex-direction: column;
	align-items: center;
	padding: 2rem 0px;
`;

export const Col3 = styled(motion.div)`
	width : 100%;
	background: var(--dark);
	display : flex;
	flex-direction: column;
	align-items: center;
	padding: 2rem 0px;
`;

export const CategoryDownload = styled.button`
	border-style: none;
	outline: none;
	padding : .5rem 1rem;
	background: var(--dark);
	border-radius: .2rem;

	&:after {
		content: "download";
		color: var(--light);
	}

	&:hover {
		background: var(--success);
	}

	&:disabled {
		background: var(--info);
	}
`;


export const CarTable = styled(motion.div)`
	
	width : 92%;
	height : auto;
	background: var(--light);
	border-radius: .2rem;
	position: relative;
	margin-top : 2rem;
	padding: 1rem;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;

	& h1 {
		font-size: var(--md);
		margin: 1rem 0px;
	}
`;

export const CarGrid = styled.div`
	width: 100%;
	min-height: 30vh;
	position: relative;
	padding-top: 3rem;

	& strong{
		color : var(--light);
		background: var(--success);
		border-radius: .2rem;
		box-shadow: 2px 2px 6px rgba(0,0,0,.5);
		padding: .5rem;
		margin-bottom: 1rem;
	}

	& p {
		color : var(--light);
		background: var(--info);
		border-radius: .2rem;
		padding: 2rem 1rem;
		margin-top: 1rem;
		margin-bottom: 1rem;
	}
`;

export const CarGridTitle = styled.div`
	position: absolute;
	top: -50px;
	right: 20%;
	left: 20%;
	background: var(--info);
	border-radius: .2rem;
	box-shadow: 2px 2px 14px rgba(0,0,0,.8);
	color: var(--light);
	text-transform : uppercase;
	font-weight: 400;
	display: flex;
	justify-content: center;
	align-items: center;
	min-height: 64px;

`;

export const Car = styled.div`
	width: 100%;
	display: flex;
	padding: 1rem;
	background: var(--info);
	box-shadow: 4px 4px 12px rgba(0,0,0,.8);
	border-radius: .2rem;
	align-items: center;
	justify-content: space-between;
	margin : 1rem 0px;

`;

export const CarImage = styled.div`
	width: 64px;
	height: 78px;
	background: var(--info);
	background-image : url(${ ({src}) => src && src });
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	position: relative;

	&:after {
		content: "Change";
		color: var(--light);
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%,-50%);
		background: rgba(0,0,0,.5);
		padding: .5rem;
		border-radius: 2rem;
		font-size: .8rem;
		display: ${ ({isEditing})=> isEditing ? "block" : "none" };
	}

	&:hover:after {
		background: var(--dark);
	}
`;

export const CarText = styled.div`
		display: grid;
		grid-template-columns: repeat(4,1fr);
		width: 60%;

	& p {
		margin-right : .5rem;
		padding: .5rem 1rem;
		text-align: center;
		color: var(--success);
	}
`;


export const CarActions = styled.div`
	& button {
		border-style: none;
		background: none;
		border-radius: .2rem;
		outline: none;
		padding: .5rem 1rem;
		border: 1px solid var(--dark);
		margin: 0px 1rem;
		color: var(--light);
	}
	
	& button:hover{
			background: var(--primary);
		}
	& button:disabled {
		background: var(--success);
		opacity: 0;
	}
`;

export const EditInput = styled.input`
	background: inherit;
	border-style: none;
	border-bottom : 1px solid var(--success);
	background-image: url(${ editImg });
	background-position: 98% 50%;
	background-size : 16px;
	background-repeat: no-repeat;
	outline: none;
	color: var(--light);
`;

export const AddCarButton = styled.button`
	width: 64px;
	height: 64px;
	border-style: none;
	outline: none;
	border-radius: 50%;
	background: var(--info);
	background-image : url(${add});
	background-size: 64px;
	background-position: center;
	background-repeat: no-repeat;
	position: absolute;
	top: -50px;
	left: 2%;
	transition: transform .2s ease-in-out;
	&:hover{
		transform : scale(1.05);
	} 
`;


