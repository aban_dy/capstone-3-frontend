import React from 'react';
import { Link } from 'react-router-dom';
import {
	Card,
	CardDetails,
	CardText,
	CardPrice,
	CardImage,
	CardButton,
} from '../HomeStyled';


const cardHoverAnimation = {
	y : -15 ,
	transition : {
		duration: 0.4,
		type: 'spring',
		stiffness: 200,
		ease: 'easeInOut'
	}
}

const childrVariants = {
	start : {
		y : '0'
	},
	end: {
		y : '50%'
	}
}


const ImageUrl = "https://capstone-3-backend.herokuapp.com/"
const CarRow = ({car,setCarDetails}) => {
	return(
		<Card 
			variants={ childrVariants }
			whileHover={ cardHoverAnimation }
		>
			<CardImage 
				src={`${ImageUrl}${car.image}`}
				onMouseEnter={ ()=> setCarDetails(car) }
			/>
			<CardDetails>
			<CardText>{ car.name }</CardText>
			<CardPrice>{`Php ${ car.price } / day`}</CardPrice>
			<Link to={ sessionStorage.token ? `/rent/${ car._id }` : `/login`}><CardButton/></Link>
			</CardDetails>
		</Card>
	)
}

export default CarRow;