import React from 'react';
import {
	Card,
	CardDetails,
	CardText,
	CardPrice,
	CardImage,
	CardButton,
} from '../HomeStyled';


const childrVariants = {
	start : {
		y : '0'
	},
	end: {
		y : '50%'
	}
}

const transition = {
	duation : .2,
	yoyo : Infinity,
	ease : "easeInOut"
}


const CarLoader = () => {
	return(
		<Card 
			variants={ childrVariants }
			transition={ transition }
		>
		</Card>
	)
}

export default CarLoader;