import React, { useState } from 'react';
import { motion } from 'framer-motion';
import styled from 'styled-components';
import landing from '../../assets/images/landing.jpg';
import orange from '../../assets/icons/orange-logo.svg';
import { Link, useHistory } from 'react-router-dom';

//Slick package
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

//Wedding car images
import weddingCar1 from '../../assets/images/wedding-car-1.jpg';
import weddingCar2 from '../../assets/images/wedding-car-2.jpg';
import weddingCar3 from '../../assets/images/wedding-car-3.jpg';

//Travel car images
import travelCar1 from '../../assets/images/travel-car-1.jpg';
import travelCar2 from '../../assets/images/travel-car-2.jpg';
import travelCar3 from '../../assets/images/travel-car-3.jpg';

//Ride car images
import rideCar1 from '../../assets/images/ride-car-1.jpg';
import rideCar2 from '../../assets/images/ride-car-2.jpg';
import rideCar3 from '../../assets/images/ride-car-3.jpg';

//Footer icons
import facebook from '../../assets/icons/icons8-facebook-32.png';
import twitter from '../../assets/icons/icons8-twitter-squared-32.png';
import instagram from '../../assets/icons/icons8-instagram-32.png';

/*
	TODOS:
	--create a state for searching queries
	--pass the query to homepage where lists of cars are displayed
	--auto complete search
*/

const Container = styled.div`
	height : 100vh;
	overflow: hidden;
	width : 100%;
	display : flex;
	flex-direction : column;
	align-items : center;
	position : relative;
	z-index : 2;
	background : url(${ landing });
	background-size : cover;
	background-position : center;
	background-attachment : fixed;
	background-repeat : no-repeat;
`;

const TopNav = styled(motion.div)`
	width : 90%;
	margin-top : 1rem;
	display : flex;
	justify-content : space-between;
	align-items : center;
	z-index : 2;
`;

const SignIn = styled.button`
	padding: .5rem 1rem;
	color : var(--light);
	border-style : none;
	border-radius : .2rem;
	outline : none;
	background : var(--danger);
	font-size : 1.2rem;

	& a {
		text-decoration : none;
		color : var(--light);
	}

	&:hover{
		cursor : pointer;
	}
`;

const Header = styled(motion.div)`
	width : 100%;
	height : 50%;
	margin-top : 4rem;
	display : flex;
	flex-direction : column;
	justify-content : center;
	align-items : center;
	z-index : 2;
	

	& h1 {
		font-size : var(--md);
		font-weight : 600;
		letter-spacing : .25rem;
		text-align : center;
		margin-bottom : 1.3rem;
	}

	& h2 {
		font-size : var(--sm);
		text-transform : lowercase;
	}
`;

const Search = styled(motion.div)`
	width : 100%;
	padding : 0 2rem;
	display : flex;
	flex-direction : column;
	align-items : center;
	justify-content : center;
	z-index : 2;


	& h2 {
		font-size: var(--sm);
		text-transform : lowercase;
		text-transform : capitalize;
		margin-top : 1.5rem;
	}

	& h2 > a {
		text-decoration : none;
		color : var(--primary); 
	}
`;
const InputContainer = styled.div`
	width : 50%;
	position: relative;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const SearchButton = styled.button`
	position: absolute;
	background: var(--danger);
	color : var(--light);
	top: 0;
	right: 0;
	bottom: 0;
	border-style : none;
	outline: none;
	padding : 0 1rem;
`;

const Input = styled.input`
	background: inherit;
	box-shadow: inset 0px 0px 100rem rgba(255,255,255,.25);
	backdrop-filter: blur(1rem);
	border-radius : .2rem;
	border-style: none;
	color : var(--light);
	font-size: var(--sm);
	width: 100%;
	padding: 1rem;

	outline: none;

	&::placeholder {
		text-align : center;
	}
`;

const Section = styled.div`
	height : 50vh;
	width : 100%;
	overflow : hidden;
	position : relative;
	background : url(${ landing });
	background-size : cover;
	background-position : center;
	background-attachment : fixed;
	background-repeat : no-repeat;

	&:before {
		position: absolute;
		content : "";
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background : rgba(0,0,0,.01);
		backdrop-filter : blur(10px);
	}

	& .slick-slider {
		min-width : 200px;
		max-width : 300px
	}
`;

const SectionWrapper = styled.div`
	width : 100%;
	height : 100%;
	border-top : 2px solid var(--info);
	display : flex;
	justify-content : space-around;
	align-items : center;
	padding : 1rem;
	position : relative;
`;

const ImageContainer = styled.div`
	width : 100%;
	height : 200px;
	display : flex;
	justify-content: center;
	align-items: center;
`;

const ImageOne = styled.div`
	width : 100%;
	height : 100%;
	background : url(${ props => props.src && props.src });
	background-size : cover;
	background-position : center;
	background-repeat : no-repeat;
	border-radius : .2rem;
`;

const ImageTwo = styled.div`
	width : 100%;
	height : 100%;
	background : url(${ props => props.src && props.src });
	background-size : cover;
	background-position : center;
	background-repeat : no-repeat;
	border-radius : .2rem;
`;

const ImageThree = styled.div`
	width : 100%;
	height : 100%;
	background : url(${ props => props.src && props.src });
	background-size : cover;
	background-position : center;
	background-repeat : no-repeat;
	border-radius : .2rem;
`;

const Text = styled.div`
	height : 100%;
	display : flex;
	flex-direction : column;
	align-items : center;
	justify-content : center;
	z-index : 1;

	& h1 {
		font-size: var(--sm);
	}
`;

//slider settings
const settings = {
	dots: false,
	arrows : false,
	infinite: true,
	autoplay: true,
	autoplaySpeed: 3000,
	adaptiveHeight: true,
	slidesToShow: 1,
  slidesToScroll: 1,
	className:"slider"
}

//animation
const pageVariants = {
	in:{
		opacity : 1,
		y: "0"
	},
	out:{
		opacity : 0,
		y: "-100vh"
	},
	init:{
		y: "100vh"
	}
}

const pageTransition = {
	duration : .2,
	type : "spring",
	stiffness : 200
}

const Landing = props => {

	const[keyword,setKeyWord] = useState("");
	let history = useHistory();

	const handleOnChage = (e) => {
		setKeyWord(e.target.value);
	} 

	const handleOnClick = () => {
		if(keyword !== ""){
			sessionStorage.keyword = keyword;
		}else{
			sessionStorage.removeItem("keyword");
		}
		history.push('/home');
	}

	return(
	<React.Fragment>
		<Container>
			<TopNav animate="in" initial="init" variants={ pageVariants } transition={ pageTransition } exit="out">
				<img src={ orange } alt="logo"/>
				<SignIn>
					<Link to="/register" >{"sign up"}</Link>
				</SignIn>
			</TopNav>
			<Header animate="in" initial="init" variants={ pageVariants } transition={ pageTransition } exit="out">
				<h1>{"Rent a car with the"}</h1>
				<h1>{"latest models,"}</h1>
				<h2>{"Choose among popular brands"}</h2>
			</Header>
			<Search animate="in" initial="init" variants={ pageVariants } transition={ pageTransition } exit="out">
				<InputContainer>
					<Input type="text" placeholder="search car here..." onChange={ handleOnChage }/>
					<SearchButton
						onClick={ handleOnClick }
					>{"Try it now!"}</SearchButton>
				</InputContainer>
				<h2>
					{"already have an account? "}
					<Link to="/login">{"sign in here"}</Link>
				</h2>
			</Search>
		</Container>
		<Section>
			<SectionWrapper>
				<Slider {...settings}>
					<ImageContainer>
						<ImageOne src={ weddingCar1 } />
					</ImageContainer>
					<ImageContainer>
						<ImageTwo src={ weddingCar2} />
					</ImageContainer>
					<ImageContainer>
						<ImageThree src={ weddingCar3 } />
					</ImageContainer>
				</Slider>
			<Text>
				<h1>{"Need a car for your wedding?"}</h1>
				<h1>{"We got you covered."}</h1>
			</Text>
			</SectionWrapper>
		</Section>
		<Section>
			<SectionWrapper>
				<Text>
					<h1>{"Travel with friends?"}</h1>
					<h1>{"Choose awesome cars"}</h1>
				</Text>
				<Slider {...settings}>
					<ImageContainer>
						<ImageOne src={ travelCar1 }/>
					</ImageContainer>
					<ImageContainer>
						<ImageTwo src={ travelCar2 }/>
					</ImageContainer>
					<ImageContainer>
						<ImageThree src={ travelCar3 } />
					</ImageContainer>
				</Slider>
			</SectionWrapper>
		</Section>
		<Section>
			<SectionWrapper>
				<Slider {...settings}>
					<ImageContainer>
						<ImageOne src={ rideCar1 } />
					</ImageContainer>
					<ImageContainer>
						<ImageTwo src={ rideCar2 }/>
					</ImageContainer>
					<ImageContainer>
						<ImageThree src={ rideCar3 } />
					</ImageContainer>
				</Slider>
			<Text>
				<h1>{"Or you just want to ride?"}</h1>
				<h1>{"We have the right car for you."}</h1>
			</Text>
			</SectionWrapper>
		</Section>
		<Section>
			<SectionWrapper>
				<h1>&copy;{" Orange Inc. 2020"}</h1>
				<div>
					<a rel="noopener noreferrer" target="_blank" href="https://www.twitter.com">
						<img src={ twitter } alt="icon"/>
					</a>
					<a rel="noopener noreferrer" target="_blank" href="https://www.facebook.com">
						<img src={ facebook } alt="icon"/>
					</a>
					<a rel="noopener noreferrer" target="_blank" href="https://www.instagram.com">
						<img src={ instagram } alt="icon"/>
					</a>
				</div>
			</SectionWrapper>
		</Section>
	</React.Fragment>
	)
}
export default Landing;