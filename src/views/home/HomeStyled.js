import styled from 'styled-components';
import { motion } from 'framer-motion';
import filter from '../../assets/icons/filter.png';

export const HomeContainer = styled.div`
	width : 100%;
	height : auto;
	display : flex;
	flex-direction : column;
	align-items : center;
	justify-content : center;
	position : relative;


`;

export const HeaderContainer = styled(motion.div)`
	height : 100vh;
	width : 100%;
	z-index : 1;

	position : fixed;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;

	background : var(--dark);
	background-image : 
			radial-gradient(circle, rgba(7,7,7,.8),rgba(7, 7, 7, .5)),
			url(${ ({src}) => src && src });
	background-repeat : no-repeat;
	background-size : cover;
	background-position : center;
	background-attachment : fixed;
	animation : fadeIn .4s easi-in-out;

	@keyframes fadeIn {
		0% : { opacity : 0 },
		50% : { opacity: .5 },
		100% : { opacity: 1 }
	}

	display: flex;
	align-items : flex-start;
	justify-content : flex-start;

	&:before {
		position: fixed;
		height: 100%;
		content: "";
		left: 0;
		bottom: 0;
		right: 0;
		background: inherit;
		box-shadow : inset 100px 0px 100rem rgba(0,0,0,1);
		z-index : 2;
	}
`;

export const HeaderDetails = styled(motion.div)`
	padding : 1rem;
	z-index : 3;
	width : 100%;
	height: 30vh;
	color : var(--light);
	display: flex;
	flex-direction: column;
	align-items : flex-end ;
	justify-content : space-between;

	& h1 {
		width: 100%;
		font-size : var(--md);
		text-transform : uppercase;
	}

	& div{
		display: flex;
		width: 100%;
	}

	& h2{
		letter-spacing : .1rem;
		line-height : 1.2rem;
		word-wrap : break-word;
		text-align : justify;
		padding: .5rem 2rem;
		margin-top: .5rem;
		margin-right: 1rem;
		text-align: center;
		border-radius: 2rem;
		background: rgba(0,0,0,.25);
		backdrop-filter: blur(2rem);
	}

`;

//Search and filter
export const HeaderControls = styled.div`
		padding : 1rem;
		z-index : 3;
		min-width : 50%;
		max-width : 50vw;
		color : var(--light);
`;

export const HeaderSearch = styled.div`
  width : 100%;
	display : flex;
	align-items : center;
	justify-content : space-around;
`;
export const HeaderInputContainer = styled.div`
	width : 60%;
	position : relative;
	margin-right: 1rem;
`;
export const HeaderInput = styled.input`
	width : 100%;
	padding : 1rem;
	border-style : none;
	outline : none;
	background: rgba(0,0,0,.3);
	backdrop-filter : blur(1rem);
	color : var(--light);
	font-size : 1.2rem;


`;

export const HeaderFilter = styled.button`
		width : 20%;
		height : 100%;
		margin : 0px 1rem;
		padding : 1rem;
		outline: none;
		border-style: none;
		border-radius : 2rem;
		background : var(--info);
		background-image : url(${ filter });
		background-size : 24px;
		background-position : 95% 50%; 
		background-repeat: no-repeat;
		color : var(--light);
		transition: all .2s ease-in-out;


		&:before {
			content: "filter";
		}

		&:hover{
			background : var(--info);
			border-radius: .2rem;
		}
`;

export const HeaderSearchButton = styled.button`
	position : absolute;
	height : 102%;
	top : 0;
	bottom : 0;
	right : 0;
	outline : none;
	border-style : none;
	background : var(--primary);
	padding : 1rem;
	color : var(--light);
	transition: all .2s ease-in-out;

	&:hover {
		background : var(--danger);
	}

`;

//end of search and filter

//Sections with horizontal scroll
export const Section = styled.div`
	width : 100%;
	height : auto;
	position : relative;
	display: flex;
	flex-direction : column;
	align-items : flex-start;
	justify-content : flex-end;
	overflow: hidden;
`;

export const Wrapper = styled(motion.div)`
	width: 100%;
	height : auto;
	padding : 1rem 0;
	z-index : 999;
	position : relative;
	display: grid;
	padding: 1rem;
	grid-template-columns: repeat(auto-fill,minmax(200px,1fr));
	grid-gap: 1rem;
	grid-template-rows: auto;
  background: rgba(0,0,0,.5);
  backdrop-filter: blur(1rem);

  & h1 {
  	text-align: center;
  	margin: 0 auto;
  }


`;



export const Card = styled(motion.div)`
	height : 100%;
	height: 250px;
	width : 200px;
	background: var(--dark);
	margin-top: -30px;
	margin-bottom: 64px;
	box-shadow : 2px 2px 12px rgba(0,0,0,.25);
	border-radius : .2rem;
	overflow: hidden;
	position : relative;
	display : flex;
	flex-direction : column;
	justify-content : flex-end;
`;

export const CardImage = styled.div`
	position: absolute;
	width : 100%;
	height : 100%;
	background-image : url(${ ({src}) => src && src });
	background-position : center;
	background-size : cover;
	background-repeat : no-repeat;
	z-index : -1;
`;


export const CardDetails = styled.div`
	width : 100%;
	height : 40%;
	background: rgba(0,0,0,.25);
	backdrop-filter: blur(1rem);
	padding: 1rem 0px;
	z-index : 1;
	display : flex;
	flex-direction: column;
	align-items : center;
	justify-content: space-around;

	& a {
		width: 80%;
	}
`;

export const CardPrice = styled.div`
	color : var(--danger);
	width: 100%;
	font-weight : 200;
	background: var(--info);
	box-shadow: 2px 2px 1rem rgba(0,0,0,.5);
	padding : 1rem;
	text-align : center;
`;

export const CardText = styled.div`
	with : 100%;
	text-align : center;
	color : var(--light);
	font-size : var(--sm);
	font-weight : 200;
`;

export const CardButton = styled.button`
	border-style : none;
	width : 100%;
	outline : none;
	padding : .5rem 1rem;
	border-radius : .2rem;
	background : var(--info);
	margin-top: 1rem;
	z-index : 1;

	&:hover {
		background : var(--dark);
		cursor : pointer;
	}

	&:after {
		content : "RENT";
		color: var(--light);
	}

`;



