import React, { useState, useEffect } from 'react';
import SideNav from '../../components/SideNav';
import Greetings from '../../components/Greetings';
import landing from '../../assets/images/landing.jpg';
import CarLoader from './car/CarLoader';
import { 
	HomeContainer,
	HeaderContainer,
	HeaderDetails,
	Section,
	Wrapper,
	HeaderControls,
	HeaderSearch,
	HeaderInputContainer,
	HeaderInput,
	HeaderSearchButton
	} from './HomeStyled';

const URL = "https://capstone-3-backend.herokuapp.com/api";
const ImageUrl = "https://capstone-3-backend.herokuapp.com/";
const CarRow = React.lazy(()=>import('./car/CarRow'));
const Home = props => {
/*----------------*/
/*-----STATES-----*/
/*----------------*/
const [carDetails,setCarDetails] = useState(null);
const [cars,setCars] = useState([]);
const [keyword,setKeyword] = useState("");
/*-------------------*/
/*-----FUNCTIONS-----*/
/*-------------------*/
useEffect(()=>{
	
	if(sessionStorage.keyword){

		fetch(`${ URL }/searchcars`,{
			method: "POST",
			headers : { "Content-type" : "application/json" },
			body : JSON.stringify({
				keyword : sessionStorage.keyword
			})
		})
		.then(res=>res.json())
		.then(res=>{
			setCars(res);
			setCarDetails(res[0]);
		});

	}else{
		fetchCars();
	}

},[sessionStorage.keyword]);

const fetchCars = async () => {
	let res = await fetch(`${ URL }/cars`);
	let result = await res.json();
	setCars(result);
	setCarDetails(result[0]);
}

const handeOnChange = (e) => {
	setKeyword(e.target.value);
}

const handleSearch = async () => {

	if(keyword === ""){
		fetchCars();
	}else{
			let res = await fetch(`${URL}/searchcars`,{
			method: "POST",
			headers : { "Content-type" : "application/json" },
			body: JSON.stringify({
				keyword : keyword
			})
		});
		if(res.status === 200){
			let result = await res.json();
			setCars(result);
			setCarDetails(result[0]);
		}
	}

}

const containerVariants = {
	start : {
		transition: {
			staggerChildren : 0.2
		}
	},
	end: {
		transition: {
			staggerChildren : 0.2
		}
	}
}

	return(
		<React.Fragment>
		{ sessionStorage.email ? <Greetings userEmail={ sessionStorage?.email } /> : "" }
		<SideNav />
		<HomeContainer>
			<HeaderContainer src={ cars.length === 0 ? landing : `${ImageUrl}${ carDetails?.image }` }/>
			<Section>
					<HeaderDetails
						exit={{opacity : 0}}
					>
						<h1>{carDetails === null ? "" : carDetails?.name}</h1>
						<div>
							<h2>{carDetails === null ? "" : carDetails?.model?.name}</h2>
							<h2>{carDetails === null ? "" : carDetails?.type?.name}</h2>
							<h2>{carDetails === null ? "" : carDetails?.price}</h2>
						</div>
						<HeaderControls>
							<HeaderSearch>
								<HeaderInputContainer>
									<HeaderInput 
										type="search" 
										placeholder="search" 
										defaultValue={ sessionStorage.keyword ? sessionStorage.keyword : "" } 
										onChange={ handeOnChange }/>
									<HeaderSearchButton
										onClick={ handleSearch }
									>{"search"}</HeaderSearchButton>
								</HeaderInputContainer>
							</HeaderSearch>
						</HeaderControls>
					</HeaderDetails>
				<Wrapper
					animate="start"
					initial="end"
					variants={ containerVariants }
				>
				<React.Suspense fallback={ <CarLoader /> }>
				{cars.length === 0 ? <h1>No cars found</h1> : cars?.map(indivCar=>(
					<CarRow 
						key={ indivCar._id } 
						car={ indivCar }
						setCarDetails={ setCarDetails }
					/>
				))}
				</React.Suspense>
				</Wrapper>
			</Section>
		</HomeContainer>
		</React.Fragment>
	)
}
export default Home;