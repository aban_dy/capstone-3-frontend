import styled from 'styled-components';
import { motion } from 'framer-motion';
import landing from '../../assets/images/landing.jpg';
import user from '../../assets/icons/user.png';


export const LoginContainer = styled(motion.div)`
	width : 100%;
	height: 100vh;
	background : url(${ landing });
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
	background-attachment : fixed;

	overflow-y : hidden;
	
	display: flex;
	justify-content : center;
	align-items : center;	

	& h1 {
		color : var(--light);
	}
	& a {
		text-decoration : none;
		color: var(--light);
	}

	& a:hover {
		color: var(--primary);
	}
`;

export const LoginForm = styled(motion.div)`
	width : 30%;
	height: 70%;
	background: rgba(255,255,255,.25);
	backdrop-filter : blur(1rem);
	border-radius: .2rem;
	
	display: flex;
	flex-direction : column;
	align-items: center;
	justify-content: space-around;

`;

export const Logo = styled.div`
	height: var(--md);
	width : var(--md);
	background: url(${ user });
	background-size : cover;
	background-position: center;
	background-repeat: no-repeat;

`;

export const InputContainer = styled.div`
	width: 80%;
	position: relative;

`;


export const Input = styled.input`
	width: 100%;
	height: var(--md);
	background: inherit;
	background-image: url(${ ({src}) => src && src });
	background-repeat: no-repeat;
	background-position: 100% 12px;
	background-size: 24px;
	border-style: none;
	border-bottom : 1px solid var(--info);

	outline: none;
	color: var(--light);

	&::placeholder {
		color: var(--light);
	}

	&:focus {
		border-bottom : 2px solid var(--danger);
	}

`;

export const Button = styled(motion.button)`
	display: inline-block;
	width: 80%;
	border-style: none;
	border-radius: .2rem;
	padding 1rem 2.5rem;
	outline: none;
	color: var(--light);
	background: var(--danger);

`;

