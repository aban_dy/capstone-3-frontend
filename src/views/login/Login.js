import React,{ useState } from 'react';
import { SuccessToast, ErrorToast } from '../../components/toast/Toast';
import { Link, useHistory } from 'react-router-dom';

import { 
	LoginContainer,
	LoginForm,
	Logo,
	InputContainer,
	Input,
	Button
	} from './LoginStyled';


//animation settings
const pageVariants = {
	in:{
		opacity : 1,
		y: "0"
	},
	out:{
		opacity : 0,
		y: "-100vh"
	},
	init:{
		y: "100vh"
	}
}

const pageTransition = {
	duration : .2,
	type : "spring",
	stiffness : 200
}
const URL = "https://capstone-3-backend.herokuapp.com/api";
const headers = {'Content-type':'application/json'};

const Login = props => {

const [email,setEmail] = useState("");
const [password,setPassword] = useState("");
const [msg,setMsg] = useState("");
let history = useHistory();

const handleLogin = async () => {
	let res = await fetch(`${URL}/login`,{
		method: "POST",
		headers : headers,
		body: JSON.stringify({
			email,
			password
		})
	});
	let result = await res.json();
	if(res.status === 200){
		sessionStorage.email = result.user.email;
		sessionStorage.isAdmin = result.user.isAdmin;
		sessionStorage.id = result.user.id;
		sessionStorage.token= result.token;
		SuccessToast();
		history.push('/home');
	}else{
		setMsg(result.message);
		ErrorToast();
	}
}

	return(
		<React.Fragment>
			<LoginContainer>
				<LoginForm 
					animate="in"
					initial="init"
					exit="out"
					variants={ pageVariants }
					transition={ pageTransition }
				>
					<Logo />
					<h1>{msg}</h1>
					<InputContainer>
						<Input 
							type="email" 
							placeholder="EMAIL" 
							autoComplete="no" 
							src={ email }
							onChange={ (e)=>setEmail(e.target.value) }
						/>
					</InputContainer>
					<InputContainer>
						<Input 
							type="password" 
							placeholder="PASSWORD" 
							autoComplete="no" 
							src={ password }
							onChange={ (e)=>setPassword(e.target.value) }
						/>
					</InputContainer>
					<Button
						whileHover={{backgroundColor:"red"}}
						onClick={ handleLogin }
					>{"LOGIN"}</Button>
						<h1>{"Need an account? "}<Link to="/register">{"Sign up here"}</Link></h1>
				</LoginForm>
			</LoginContainer>
		</React.Fragment>
	)
}

export default Login;