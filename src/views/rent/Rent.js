import React, { useState,useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { SuccessToast,ErrorToast } from '../../components/toast/Toast';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import moment from 'moment';
import SideNav from '../../components/SideNav';


//styled-componets
import { 
	RentContainer,
	DateContainer,
	DateButtonContainer,
	DateResetButton,
	DateSumbitButton,
	CarDetailsContainer,
	ImageContainer,
	CardBadgeContainer,
	CardDetails,
	CardPrice,
	CardText,
	CardBadge
}from './RentStyled'
const URL = "https://capstone-3-backend.herokuapp.com/api";
const headers = { 'Content-type' : 'application/json' };
const imgURL = "https://capstone-3-backend.herokuapp.com";


const Rent = ({match}) => {
/*----------------*/
/*-----STATES-----*/
/*----------------*/
const [ date, setDate ] = useState({
	from : undefined,
	to: undefined,
}); 
const [car,setCar] = useState({});
const [disabledDates, setDisabledDates] = useState([]);
const [disabledArray,setDisabledArray] = useState([]);
let history = useHistory();

/*-------------------*/
/*-----FUNCTIONS-----*/
/*-------------------*/
useEffect(()=>{
	fetchData();
	fetchRent();
},[]);

const fetchRent = async () => {
	let res = await fetch(`${URL}/rent/${match.params.id}`);
	let result = await res.json();
	let newDisableDates = [];
	await result.forEach(res=>{
			newDisableDates.push({
				from: moment(res.date.from).toDate(),
				to: moment(res.date.to).toDate()
			})
	})
	setDisabledDates(newDisableDates);
}

const fetchData = async () => {
	let res = await fetch(`${ URL }/car/${match.params.id}`);
	let result = await res.json();
	setCar(result);
}

const handleSaveDate = async () => {

	let disabled = disabledDates.map(dis => dis.from.getDate());
	let diff = await moment(date.to).diff(moment(date.from),'days')+1;
	let amount = (diff * car.price);
	if(disabledArray.length === 0){

		if(date.from !== undefined){
			let res = await fetch(`${URL}/addrent`,{
				method: "POST",
				headers: headers,
				body: JSON.stringify({
					date : {
						from: date.from,
						to: date.to === undefined ? date.from : date.to
					},
					user : sessionStorage.id,
					car : match.params.id,
					amount : parseFloat(amount)
				})
			});
			if(res.status === 200){
				let result = await res.json();
				let newDisableDate = {
					from : moment(result.date.from).toDate(),
					to: moment(result.date.to).toDate()
				}

				let newDisableDates = [...disabledDates,newDisableDate];
				setDisabledDates(newDisableDates);
				SuccessToast();
				history.push('/dashboard');

			}else{
				ErrorToast();
			}
		}else{
			ErrorToast();
		}

		handeResetClick();
	}else{
		ErrorToast();
	}

}


const handleDayClick = (day,modifiers = {}) => {


		const range = DateUtils.addDayToRange(day, date);
		setDate(range);

		let daysList = [];
    if (range.from && range.to) {
      daysList = [range.from];
      while (daysList[daysList.length - 1] < range.to) {
        let day = daysList[daysList.length - 1];
        daysList.push(
          new Date(day.getFullYear(), day.getMonth(), day.getDate() + 1),
        );
      }
      daysList.pop();
    }

    let result = [];
    disabledDates.forEach(dis => {
      	daysList.forEach(list => {
      		if(list.getDate() === dis.from.getDate()){
      			result.push(list);
      		}
      	})
      });

    setDisabledArray(result);
}

const handeResetClick = () => {
	setDate({});
}

	/*
	TODOS
	--Check if user is signed-in , if not, do not display calendar
	--fetch all rents with the given car id
	--push all dates to disabledDates array
	--date is an object with from and to properties inside
	*/

	const { from, to } = date;
	const modifiers = { start : from, end: to }
	const modifiersStyles = {
		selected : {
			backgroundColor : "var(--primary)",
			outline: "none"
		},
		disabled : {
			backgroundColor : "var(--info)"
		}
	}

	//animation settings
	const pageVariants = {
	in:{
		opacity : 1,
		x: "0"
	},
	out:{
		opacity : 0,
		x: "-100vw"
	},
	init:{
		x: "100vw"
	}
}

const pageTransition = {
	duration : .2,
	type : "spring",
	stiffness : 200
}

	return(
	<React.Fragment>
		<SideNav />
		<RentContainer>
			<CarDetailsContainer
				animate="in"
				exit="out"
				initial="out"
				variants={ pageVariants }
				transition={ pageTransition }
			>
				<CardDetails>
				<CardText>{car?.name}</CardText>
				<ImageContainer src={`${imgURL}/${car.image}`}  />
					<CardPrice>{`Php ${car?.price ? car.price : ""} / day`}</CardPrice>
				<CardBadgeContainer>
					<CardBadge>{car.model?.name}</CardBadge>
					<CardBadge>{car.type?.name}</CardBadge>
				</CardBadgeContainer>
				</CardDetails>
			</CarDetailsContainer>
			<CarDetailsContainer>
			<DateContainer
				animate="in"
				exit="out"
				initial="out"
				variants={ pageVariants }
				transition={ pageTransition }
			>
				<h1>{"Choose date"}</h1>
				<DayPicker 
					className="Selectable"
					numberOfMonths = { 1 }
					fromMonth = { new Date() }
					selectedDays={[from, { from,to }]}
					modifiers={ modifiers }
					modifiersStyles={ modifiersStyles }
					onDayClick={ handleDayClick }
					disabledDays={[{before: new Date},...disabledDates]}
				/>
				<strong>
					{ moment(date.from).format('ll') } {" - "} { moment(date.to).format('ll') }
				</strong>
				<DateButtonContainer>
					<DateSumbitButton 
						onClick={ handleSaveDate }
					/>
					<DateResetButton onClick={ handeResetClick }/>
				</DateButtonContainer>
			</DateContainer>
			</CarDetailsContainer>
		</RentContainer>
	</React.Fragment>
	)
}

export default Rent;