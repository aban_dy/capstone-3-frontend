import styled from 'styled-components';
import { motion } from 'framer-motion';


export const RentContainer = styled.div`
	width: 100%;
	height: 100vh;
	background : black;
	display: flex;
	align-items: center;
	justify-content: center;
	overflow-x : hidden;
`;

export const DateContainer = styled(motion.div)`
	padding: 1rem;
	background: rgba(255,255,255,.25);
	backdrop-filter: blur(1rem);
	border-radius: .2rem;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;

	& h1 {
		font-size: var(--sm);
		margin: 1rem;
	}
	& strong {
		margin: 1rem;
	}

`;

export const DateButtonContainer = styled.div`
		width : 100%;
		display : flex;
		justify-content : space-around;
		align-items: center;
`;

export const DateResetButton = styled.button`
		background: var(--info);
		border-style: none;
		border-radius: 2rem;
		color: var(--light);
		padding: .6rem 2rem;
		outline: none;

		&:after {
			content: "RESET";
		}

		&:hover {
			background: var(--dark);
		}
`;

export const DateSumbitButton = styled.button`
 		background: var(--danger);
		border-style: none;
		border-radius: 2rem;
		color: var(--light);
		padding: .5rem 2rem;
		outline: none;

		&:after {
			content: "SUBMIT";
		}

		&:hover {
			background: var(--primary);
		}
`;

//Cars
export const CarDetailsContainer = styled(motion.div)`
	width : 50%;
	height : 100%;
	display: flex;
	flex-direction : column;
	justify-content: center;
	align-items: center;
`;

export const ImageContainer = styled.div`
	width : 80%;
	height : 50%;
	background-image : url(${ ({src}) => src && src });
	background-size : 100%;
	background-repeat: no-repeat;
	background-position: center;
`;

export const CardDetails = styled.div`
	width : 100%;
	height : 100%;
	background: rgba(0,0,0,.05);
	backdrop-filter: blur(1rem);
	z-index : 1;
	display : flex;
	flex-direction: column;
	align-items : center;
	justify-content: center;

`;

export const CardPrice = styled.div`
	color : var(--danger);
	width: 75%;
	font-weight : 200;
	border-radius : 2rem;
	border : 1px solid var(--info);
	padding : 1rem;
	text-align : center;
	margin: 1rem 0px;
`;

export const CardText = styled.div`
	width : 100%;
	color : var(--light);
	font-size : var(--md);
	font-weight : 400;
	margin: 1rem 0;
	text-align: center;
`;

export const CardBadgeContainer = styled.div`
	width: 85%;
	display: flex;
`;

export const CardBadge = styled.div`
	padding : .5rem 2rem;
	background : inherit;
	border : 1px solid var(--info);
	border-radius: 2rem;
	background: var(--secondary);
	margin : 1rem;
`;