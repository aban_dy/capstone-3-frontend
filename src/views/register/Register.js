import React,{ useState } from 'react';
import { SuccessToast, ErrorToast } from '../../components/toast/Toast';
import { Link,useHistory } from 'react-router-dom';

import { 
	RegisterContainer,
	RegisterForm,
	Logo,
	InputContainer,
	Input,
	Button
	} from './RegisterStyled';


//animation settings
const pageVariants = {
	in:{
		opacity : 1,
		y: "0"
	},
	out:{
		opacity : 0,
		y: "-100vh"
	},
	init:{
		y: "100vh"
	}
}

const pageTransition = {
	duration : .2,
	type : "spring",
	stiffness : 200
}

const URL = "https://capstone-3-backend.herokuapp.com/api";
const headers = {'Content-type':'application/json'};

const Register = props => {

const [firstName,setFirstName] = useState("");
const [lastName,setLastName] = useState("");
const [email,setEmail] = useState("");
const [password,setPassword] = useState("");
const [msg,setMsg] = useState("");
let history = useHistory();

const handleRegister = async () => {
	let res = await fetch(`${URL}/register`,{
		method: "POST",
		headers : headers,
		body: JSON.stringify({
			firstName,
			lastName,
			email,
			password
		})
	});
	if(res.status === 200){
		SuccessToast();
		history.push('/login');
	}else{
		ErrorToast();
		let result = await res.json();
		setMsg(result.message);
	}
}

	return(
		<React.Fragment>
			<RegisterContainer>
				<RegisterForm 
					animate="in"
					initial="init"
					exit="out"
					variants={ pageVariants }
					transition={ pageTransition }
				>
					<Logo />
					<h1>{msg}</h1>
					<InputContainer>
						<Input 
							type="text"
							placeholder="FIRST NAME" 
							autoComplete="no"
							onChange={ (e)=>setFirstName(e.target.value) } 
						/>
					</InputContainer>
						<InputContainer>
						<Input 
							type="text" 
							placeholder="LAST NAME" 
							autoComplete="no"
							onChange={ (e)=>setLastName(e.target.value) } 
						/>
					</InputContainer>
					<InputContainer>
						<Input 
							type="email" 
							placeholder="EMAIL" 
							autoComplete="no" 
							src={ email }
							onChange={ (e)=>setEmail(e.target.value) }
						/>
					</InputContainer>
					<InputContainer>
						<Input 
							type="password" 
							placeholder="PASSWORD" 
							autoComplete="no" 
							src={ password }
							onChange={ (e)=>setPassword(e.target.value) }
						/>
					</InputContainer>
					<Button
						whileHover={{backgroundColor:"red"}}
						onClick={ handleRegister }
					>{"REGISTER"}</Button>
					<h1>
						{"already have an account? "}<Link to="/login">{"Sign in here"}</Link>
					</h1>
				</RegisterForm>
			</RegisterContainer>
		</React.Fragment>
	)
}

export default Register;