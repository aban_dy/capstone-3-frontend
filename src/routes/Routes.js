import React from 'react';
import {
	Switch,
	Route,
	useLocation
	} from 'react-router-dom';
import Loader from '../components/Loader';
import { AnimatePresence } from 'framer-motion';


//import components to be rendered via React.lazy()
const Landing = React.lazy(()=> import('../views/home/Landing'));
const Home = React.lazy(()=> import('../views/home/Home'));
const Login = React.lazy(()=> import('../views/login/Login'));
const Register = React.lazy(()=> import('../views/register/Register'));

const Routes = props => {

	let location = useLocation();

	return(
		<React.Suspense fallback= { <Loader /> } >
			<AnimatePresence exitBeforeEnter>
				<Switch location={ location } key={ location.pathname }>
					<Route exact path="/" render={ props => <Landing { ...props } /> }/>
					<Route path="/home" render={ props => <Home { ...props } /> }/>
					<Route path="/login" render={ props => <Login { ...props } /> }/>
					<Route path="/register" render={ props => <Register { ...props } /> }/>
				</Switch>
			</AnimatePresence>
		</React.Suspense>
	)
}
export default Routes;